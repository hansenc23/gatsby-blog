import React from "react";
import { Link, graphql } from "gatsby";
import Layout from "../components/layout";
import SEO from "../components/seo";
import "./blogpost.css";
const BlogPost = ({ data }) => {
  console.log(data.contentfulBlogPost);
  const { title, body, image, tags, contentGrid } = data.contentfulBlogPost;
  return (
    <Layout>
      <SEO title={title} />
      <div className="blogpost">
        <h1>{title}</h1>
        <img alt={title} src={image.file.url} />
        <div className="tags">
          {tags.map(tag => (
            <span className="tag" key={tag}>
              {tag}
            </span>
          ))}
        </div>
        <p className="body-text">{body.body}</p>

        <div className={`content-grid column-${contentGrid.columns}`}>
          {contentGrid &&
            contentGrid.contentItem.map((item, i) => {
              if (item.__typename === "ContentfulImageItem") {
                return (
                  <img
                    key={i}
                    className="content-item"
                    src={item.image.file.url}
                  />
                );
              } else if (item.__typename === "ContentfulImageWithText") {
                return (
                  <div key={i} className="content-item">
                    <img src={item.image.file.url} />
                    <h2>{item.header}</h2>
                    <p>{item.description.description}</p>
                  </div>
                );
              }
            })}
        </div>
        <Link to="/blogposts">View more posts</Link>
        <Link to="/">Back to Home</Link>
      </div>
    </Layout>
  );
};
export default BlogPost;
export const pageQuery = graphql`
  query($slug: String!) {
    contentfulBlogPost(slug: { eq: $slug }) {
      title
      slug
      body {
        body
      }
      image {
        file {
          url
        }
      }
      tags
      contentGrid {
        contentItem {
          ... on ContentfulImageWithText {
            header
            description {
              description
            }
            image {
              file {
                url
              }
            }
          }
          ... on ContentfulImageItem {
            image {
              file {
                url
              }
            }
          }
        }
        columns
      }
    }
  }
`;
